import React, { Component } from 'react';
import './App.css';
import List from './List.js';
import Items from './Items.js';

class App extends Component {
  constructor() {
    super();
    this.state = {
      items: [{text:'Sus tareas se verán aquí...', key:'-1'}],
      currentItem: {text:'', key:''},
      colorTitle: 'white'
    }
    this.inputElement=React.createRef();
    setTimeout(() => console.log("asd"),5000);
    setTimeout(() => {
      this.setState({ colorTitle: 'whitesmoke' });
    }, 5000);
  }
  handleInput = e => {
    console.log('Hello Input')
  }
  addItem = () => {
    console.log('Hello Add Item')
  }
  handleInput = e => {
    const itemText = e.target.value
    const currentItem = { text: itemText, key: Date.now() }
    this.setState({
      currentItem,
    })
  }
  addItem = e => {
    e.preventDefault()
    const newItem = this.state.currentItem
    if (newItem.text !== '') {
      console.log(newItem);
      const items =  (this.state.items[0].key==='-1') ?  [newItem] : [...this.state.items, newItem]; 
      this.setState({
        items: items,
        currentItem: { text: '', key: '' },
      })
    }
  }
  render(){
    const style = {
      color: this.state.colorTitle,
      textAlign: 'center'
    }    
    return (
      <div>
        <h1 style={style}>TodoList!</h1>
        <div className="App">
          <List 
            addItem={this.addItem}
            inputElement={this.inputElement}
            handleInput={this.handleInput}
            currentItem={this.state.currentItem}
          ></List>
          <Items
            listItems={this.state.items}
          ></Items>
        </div>
      </div>
    );
  }
}

export default App;
