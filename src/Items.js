import React, { Component } from 'react'

class TodoItems extends Component {
  createTasks(item) {
    return <li key={item.key}>{item.text}</li>
  }
  render() {
    const todoEntries = this.props.listItems;
    return (
        <div className="Results">
            { todoEntries.map( entry => 
                <ul className={`theList ${entry.key==='-1' ? 'placeholder' : ''}`} key={entry.key}><input type="checkbox"></input>{entry.text}</ul>
            )}
        </div>)
  }
}
export default TodoItems